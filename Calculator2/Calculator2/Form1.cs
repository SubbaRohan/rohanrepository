﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Calculator2
{
    public partial class Form1 : Form
    {
        Double resultValue = 0;
        String operationPerformed = "";
        bool isOperationPerformed = false;

        public Form1()
        {
            InitializeComponent();
        }

        private void button1(object sender, EventArgs e)
        {
            
        }

        private void button_click(object sender, EventArgs e)
        {
            if ((txtDisplay.Text == "0") || (isOperationPerformed))
                txtDisplay.Clear();

            isOperationPerformed = false;
            Button b = (Button)sender;
            if(b.Text==".")
            {
                if(!txtDisplay.Text.Contains("."))
                    txtDisplay.Text = txtDisplay.Text + b.Text;
            }
            else
            txtDisplay.Text = txtDisplay.Text + b.Text;
        
        }

        private void operator_click(object sender, EventArgs e)
        {
            Button b = (Button)sender;

            if (resultValue != 0)
            {
                button12.PerformClick();
                operationPerformed = b.Text;
                lbloperation.Text = resultValue + " " + operationPerformed;
                isOperationPerformed = true;
            }
            else 
            { 
                operationPerformed = b.Text;
                resultValue = Double.Parse(txtDisplay.Text);
                lbloperation.Text = resultValue + " " + operationPerformed;
                isOperationPerformed = true;
            }
        }

        private void button6_Click(object sender, EventArgs e)
        {
            txtDisplay.Text = "0";
            resultValue = 0;
        }

        private void button12_Click(object sender, EventArgs e)
        {
            switch(operationPerformed)
            {
                case "+":
                    txtDisplay.Text = (resultValue + Double.Parse(txtDisplay.Text)).ToString();
                    break;
                case "-":
                    txtDisplay.Text = (resultValue - Double.Parse(txtDisplay.Text)).ToString();
                    break;
                case "*":
                    txtDisplay.Text = (resultValue * Double.Parse(txtDisplay.Text)).ToString();
                    break;
                case "/":
                    txtDisplay.Text = (resultValue / Double.Parse(txtDisplay.Text)).ToString();
                    break;
                default:
                    break;
            }
            resultValue = Double.Parse(txtDisplay.Text);
            lbloperation.Text = "";
        }
    }
}
