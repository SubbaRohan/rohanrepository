﻿namespace combobox
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btn_pen = new System.Windows.Forms.Button();
            this.btn_square = new System.Windows.Forms.Button();
            this.btn_rectangle = new System.Windows.Forms.Button();
            this.btn_color = new System.Windows.Forms.Button();
            this.btn_canvas = new System.Windows.Forms.Button();
            this.btn_circle = new System.Windows.Forms.Button();
            this.btn_comboBox = new System.Windows.Forms.ComboBox();
            this.text_ShapeSize = new System.Windows.Forms.TextBox();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(800, 28);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(46, 24);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(55, 24);
            this.helpToolStripMenuItem.Text = "Help";
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(12, 147);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(776, 291);
            this.panel1.TabIndex = 1;
            this.panel1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.panel1_MouseDown);
            this.panel1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.panel1_MouseMove);
            this.panel1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.panel1_MouseUp);
            // 
            // btn_pen
            // 
            this.btn_pen.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.btn_pen.FlatAppearance.BorderSize = 0;
            this.btn_pen.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_pen.Location = new System.Drawing.Point(59, 51);
            this.btn_pen.Name = "btn_pen";
            this.btn_pen.Size = new System.Drawing.Size(108, 31);
            this.btn_pen.TabIndex = 2;
            this.btn_pen.Text = "Set Pen";
            this.btn_pen.UseVisualStyleBackColor = false;
            this.btn_pen.Click += new System.EventHandler(this.btn_pen_Click);
            // 
            // btn_square
            // 
            this.btn_square.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.btn_square.FlatAppearance.BorderSize = 0;
            this.btn_square.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_square.Location = new System.Drawing.Point(59, 100);
            this.btn_square.Name = "btn_square";
            this.btn_square.Size = new System.Drawing.Size(108, 28);
            this.btn_square.TabIndex = 2;
            this.btn_square.Text = "Square";
            this.btn_square.UseVisualStyleBackColor = false;
            this.btn_square.Click += new System.EventHandler(this.btn_square_Click);
            // 
            // btn_rectangle
            // 
            this.btn_rectangle.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.btn_rectangle.FlatAppearance.BorderSize = 0;
            this.btn_rectangle.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_rectangle.Location = new System.Drawing.Point(257, 100);
            this.btn_rectangle.Name = "btn_rectangle";
            this.btn_rectangle.Size = new System.Drawing.Size(108, 29);
            this.btn_rectangle.TabIndex = 2;
            this.btn_rectangle.Text = "Rectangle";
            this.btn_rectangle.UseVisualStyleBackColor = false;
            this.btn_rectangle.Click += new System.EventHandler(this.btn_rectangle_Click);
            // 
            // btn_color
            // 
            this.btn_color.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.btn_color.FlatAppearance.BorderSize = 0;
            this.btn_color.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_color.Location = new System.Drawing.Point(257, 51);
            this.btn_color.Name = "btn_color";
            this.btn_color.Size = new System.Drawing.Size(108, 32);
            this.btn_color.TabIndex = 2;
            this.btn_color.Text = "Pen Color";
            this.btn_color.UseVisualStyleBackColor = false;
            // 
            // btn_canvas
            // 
            this.btn_canvas.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.btn_canvas.FlatAppearance.BorderSize = 0;
            this.btn_canvas.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_canvas.Location = new System.Drawing.Point(445, 51);
            this.btn_canvas.Name = "btn_canvas";
            this.btn_canvas.Size = new System.Drawing.Size(108, 32);
            this.btn_canvas.TabIndex = 2;
            this.btn_canvas.Text = "Canvas Color";
            this.btn_canvas.UseVisualStyleBackColor = false;
            this.btn_canvas.Click += new System.EventHandler(this.btn_canvas_Click);
            // 
            // btn_circle
            // 
            this.btn_circle.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.btn_circle.FlatAppearance.BorderSize = 0;
            this.btn_circle.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_circle.Location = new System.Drawing.Point(445, 100);
            this.btn_circle.Name = "btn_circle";
            this.btn_circle.Size = new System.Drawing.Size(108, 30);
            this.btn_circle.TabIndex = 2;
            this.btn_circle.Text = "Circle";
            this.btn_circle.UseVisualStyleBackColor = false;
            this.btn_circle.Click += new System.EventHandler(this.btn_circle_Click);
            // 
            // btn_comboBox
            // 
            this.btn_comboBox.FormattingEnabled = true;
            this.btn_comboBox.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10"});
            this.btn_comboBox.Location = new System.Drawing.Point(635, 49);
            this.btn_comboBox.Name = "btn_comboBox";
            this.btn_comboBox.Size = new System.Drawing.Size(121, 24);
            this.btn_comboBox.TabIndex = 3;
            // 
            // text_ShapeSize
            // 
            this.text_ShapeSize.Location = new System.Drawing.Point(635, 100);
            this.text_ShapeSize.Multiline = true;
            this.text_ShapeSize.Name = "text_ShapeSize";
            this.text_ShapeSize.Size = new System.Drawing.Size(121, 25);
            this.text_ShapeSize.TabIndex = 4;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.text_ShapeSize);
            this.Controls.Add(this.btn_comboBox);
            this.Controls.Add(this.btn_circle);
            this.Controls.Add(this.btn_canvas);
            this.Controls.Add(this.btn_color);
            this.Controls.Add(this.btn_rectangle);
            this.Controls.Add(this.btn_square);
            this.Controls.Add(this.btn_pen);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btn_pen;
        private System.Windows.Forms.Button btn_square;
        private System.Windows.Forms.Button btn_rectangle;
        private System.Windows.Forms.Button btn_color;
        private System.Windows.Forms.Button btn_canvas;
        private System.Windows.Forms.Button btn_circle;
        private System.Windows.Forms.ComboBox btn_comboBox;
        private System.Windows.Forms.TextBox text_ShapeSize;
    }
}

